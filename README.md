# Auto CICD
In order to use the CICD, define the following variables in the ```gitlab-ci.yml``` file:

* ```PGHOST_ALIAS:```
* ```PGUSER```
* ```RAILS_ENV```
* ```POSTGRES_USER```
* ```POSTGRES_DB```
* ```POSTGRES_PASSWORD```
* ```DOCKERFILE_PATH```

# Local development using Docker
In order to build the project locally using Docker, provide the variables values and the image url (project's registry) in the ```docker-compose.yml``` file.